const express = require("express");
const mongoose = require("mongoose");
const dbConfig = require("./config");

// Routes
const postRoutes = require("./routes/api/posts");
const app = express();

//BodyParser Middleware
app.use(express.json());

// MongoDB connection
mongoose.Promise = global.Promise;
mongoose
  .connect(dbConfig.db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(
    () => {
      console.log("Database connected");
    },
    (error) => {
      console.log("Database can't be connected: " + error);
    }
  );

// User routes
app.use("/api/posts", postRoutes);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server run at port ${PORT}`));
